import Vue from 'vue';
import Router from 'vue-router';
import Dashborad from '@/modules/Dashboard/routes';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    ...Dashborad,
  ],
});
